import imaplib
import re

import time
from email import message_from_bytes
import os
from django.core import mail
from selenium.webdriver.common.keys import Keys

from .base import FunctionalTest

SUBJECT = 'Your login link for Superlists'


class LoginTest(FunctionalTest):

    def wait_for_email(self, test_email, subject):
        if not self.staging_server:
            email = mail.outbox[0]
            self.assertIn(test_email, email.to)
            self.assertEqual(email.subject, subject)
            return email.body

        start = time.time()
        email_id = None
        connection = imaplib.IMAP4_SSL('imap.gmail.com')
        try:
            connection.login(test_email, os.environ.get('RECEIVE_EMAIL_PASSWORD'))
            typ, messages = connection.select('INBOX')
            while time.time() - start < 30:
                email_id = messages[0].split()[0]
                data = connection.fetch(email_id, '(RFC822)')
                msg = message_from_bytes(data[1][0][1])
                self.assertIn(test_email, msg['To'])
                self.assertEqual(subject, msg['Subject'])
                return msg.get_payload()
        finally:
            if email_id:
                connection.store(email_id, '+FLAGS', r'(\Deleted)')
            connection.close()
            connection.logout()

    def test_can_get_email_link_to_log_in(self):
        # Edith goes to the awesome superlists site
        # and notices a "Log in" section in the navbar for the first time
        # It's telling her to enter her email address, so she does
        if self.staging_server:
            test_email = 'todoo671@gmail.com'
        else:
            test_email = 'edith@example.com'

        self.browser.get(self.live_server_url)
        self.browser.find_element_by_name('email').send_keys(test_email)
        self.browser.find_element_by_name('email').send_keys(Keys.ENTER)

        # A message appears telling her an email has been sent
        self.wait_for(lambda: self.assertIn(
            'Check your email',
            self.browser.find_element_by_tag_name('body').text
        ))

        # She checks her email and finds a message
        body = self.wait_for_email(test_email, SUBJECT)

        # It has a url link in it
        self.assertIn('Use this link to log in', body)
        url_search = re.search(r'http://.+/.+$', body)
        if not url_search:
            self.fail(f'Could not find url in email body:\n{body}')
        url = url_search.group(0)
        self.assertIn(self.live_server_url, url)

        # She clicks it
        self.browser.get(url)

        # She logged in!
        self.wait_to_be_logged_in(email=test_email)

        # Now she logs out
        self.browser.find_element_by_link_text('Log out').click()

        # She is logged out
        self.wait_to_be_logged_out(email=test_email)
