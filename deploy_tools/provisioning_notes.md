Provisioning a new site
=======================

## Required packages:

* nginx
* Python 3.6
* virtualenv + pip
* Git

eg, on Ubuntu:

    sudo add-apt-repository ppa:fkrull/deadsnakes
    sudo apt-get install nginx git python36 python3.6-venv

## Nginx Virtual Host config

* see nginx.template.conf
* replace SITENAME with, e.g., staging.my-domain.com

## Systemd service

* see gunicorn-systemd.template.service
* replace SITENAME with, e.g., staging.my-domain.com
* replace SEKRIT with email password

## Folder structure:
Assume we have a user account at /home/username

/home/username
└── sites
    └── SITENAME
         ├── database
         ├── source
         ├── static
         └── virtualenv

```
sed "s/SITENAME/staging.todoo.bid/g" source/deploy_tools/nginx.template.conf | sudo tee /etc/nginx/sites-available/staging.todoo.bid
sudo ln -s /etc/nginx/sites-available/staging.todoo.bid /etc/nginx/sites-enabled/staging.todoo.bid
sed "s/SITENAME/staging.todoo.bid/g" source/deploy_tools/gunicorn-systemd.template.service | sudo tee /etc/systemd/system/gunicorn-staging.todoo.bid.service
sudo systemctl daemon-reload
sudo systemctl reload nginx
sudo systemctl enable gunicorn-staging.todoo.bid.service
sudo systemctl start gunicorn-staging.todoo.bid.service
```